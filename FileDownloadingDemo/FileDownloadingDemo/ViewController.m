//
//  ViewController.m
//  FileDownloadingDemo
//
//  Created by Ruslan Shevtsov on 3/11/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import "ViewController.h"
#import <FileDownloading/FileLoader.h>

static NSString * const kImageDownloadingURL = @"https://ru.wallpaper.mob.org/image/downloadImage?id=47351&l=381&t=0&r=609&b=495&s=0.707395498392283";

@interface ViewController() <FileLoaderDelegate>
    
    @property (weak, nonatomic) IBOutlet UIImageView *imageView;
    @property (weak, nonatomic) IBOutlet UILabel *errorLabel;
    
    @property (strong, nonatomic) FileLoader *fileLoader;
    
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.fileLoader = nil;
}

#pragma mark -
#pragma mark Actions

- (IBAction)testAction:(id)sender {
    self.errorLabel.text = nil;
    if (self.fileLoader == nil) {
        self.fileLoader = [[FileLoader alloc] initWith:[NSURL URLWithString:kImageDownloadingURL] delegate:self];
        [self.fileLoader startDownloading];
    }
}

#pragma mark -
#pragma mark FileLoaderDelegate
    
- (void)fileLoader:(FileLoader *)loader didFailWith:(NSError *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.errorLabel.text = [error localizedDescription];
        self.fileLoader = nil;
    });
}
    
- (void)fileLoader:(FileLoader *)loader didDownloadFile:(NSURL *)fileURL {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSData *data = [NSData dataWithContentsOfURL:fileURL];
        UIImage *image = [[UIImage alloc] initWithData:data];
        self.imageView.image = image;
        self.fileLoader = nil;
    });
}

@end
