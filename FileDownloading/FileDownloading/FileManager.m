//
//  FileManager.m
//  FileDownloading
//
//  Created by Ruslan Shevtsov on 3/11/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import "FileManager.h"

@implementation FileManager

+ (NSError *)saveData:(NSData *)fileData toFileURL:(NSURL *)fileURL {
    NSError *error = nil;
    [fileData writeToFile: fileURL.path options: NSDataWritingAtomic error: &error];
    return error;
}
    
+ (NSURL *)fileURLFor:(NSData *)fileData {
    NSString *fileType = [self mimeTypeForData:fileData] ;
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    NSInteger timestamp = [[NSDate date] timeIntervalSince1970];
    NSString *fileName = [NSString stringWithFormat:@"file_%ld", (long)timestamp];
    return [NSURL fileURLWithPath: [NSString stringWithFormat:@"%@/%@.%@", documentsDirectory, fileName, fileType]];
}
    
+ (NSString *)mimeTypeForData:(NSData *)data {
    uint8_t c;
    [data getBytes:&c length:1];
    
    switch (c) {
        case 0xFF:
        return @"jpeg";
        break;
        case 0x89:
        return @"png";
        break;
        case 0x47:
        return @"gif";
        break;
        case 0x49:
        case 0x4D:
        return @"tiff";
        break;
        case 0x25:
        return @"pdf";
        break;
        case 0xD0:
        return @"vnd";
        break;
        case 0x46:
        return @"txt";
        break;
        default:
        return @"uknown";
    }
    return nil;
}
    
@end
