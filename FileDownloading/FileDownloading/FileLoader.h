//
//  FileLoader.h
//  FileLoader
//
//  Created by Ruslan Shevtsov on 3/11/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class FileLoader;

@protocol FileLoaderDelegate

@optional

- (void)fileLoader:(FileLoader *)loader didFailWith:(NSError *)error;
- (void)fileLoader:(FileLoader *)loader didDownloadFile:(NSURL *)fileURL;

@end

@interface FileLoader : NSObject

- (instancetype)initWith:(NSURL *)url delegate:(id<FileLoaderDelegate>)delegate;
- (void)startDownloading;

@end

NS_ASSUME_NONNULL_END
