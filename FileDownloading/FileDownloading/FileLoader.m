//
//  FileLoader.m
//  FileLoader
//
//  Created by Ruslan Shevtsov on 3/11/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import "FileLoader.h"
#import "FileManager.h"

@interface FileLoader ()

@property (assign, nonatomic) id<FileLoaderDelegate> delegate;
@property (nonatomic, strong) NSURL *url;

@end

@implementation FileLoader

- (instancetype)initWith:(NSURL *)url delegate:(id<FileLoaderDelegate>)delegate {
    if (self = [super init]) {
        self.url = url;
        self.delegate = delegate;
    }
    return self;
}

- (void)startDownloading {
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL: self.url];
    
    [urlRequest setHTTPMethod:@"GET"];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest: urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        if (httpResponse.statusCode == 200) {
            NSURL *fileURL = [FileManager fileURLFor: data];
            NSError *savingError = [FileManager saveData:data toFileURL: fileURL];
            if (savingError == nil) {
                [self.delegate fileLoader:self didDownloadFile:fileURL];
            } else {
                [self.delegate fileLoader:self didFailWith:savingError];
            }
        } else {
            [self.delegate fileLoader:self didFailWith:error];
        }
    }];
    [dataTask resume];
}

@end
