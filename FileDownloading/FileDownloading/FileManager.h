//
//  FileManager.h
//  FileDownloading
//
//  Created by Ruslan Shevtsov on 3/11/19.
//  Copyright © 2019 Ruslan Shevtsov. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FileManager : NSObject

+ (NSError *)saveData:(NSData *)fileData toFileURL:(NSURL *)fileURL;
+ (NSURL *)fileURLFor:(NSData *)fileData;
    
@end

NS_ASSUME_NONNULL_END
